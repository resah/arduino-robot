/*
 * Movable.h
 *
 *  Created on: Jun 29, 2015
 *      Author: resah
 * Description: Provides methods to move servos.
 */

#ifndef MOVABLE_H_
#define MOVABLE_H_

#include <Arduino.h>
#include <math.h>
#include "Performable.h"
#include "Temper.h"
#include "Perlin.h"

#define SERVOMIN  150 // this is the 'minimum' pulse length count (out of 4096)
#define SERVOMAX  600 // this is the 'maximum' pulse length count (out of 4096)

class Movable {

  private:
        Adafruit_PWMServoDriverForTrinket pwm;
        byte servoId = 0;

  public:
        Movable(Adafruit_PWMServoDriverForTrinket &servoDriver, byte servoIdentifier) {
              pwm = servoDriver;
              servoId = servoIdentifier;
        }

  protected:
        void moveToAngle(float angle) {
                int pulselength = getPulseLengthForDegrees(angle);

//                Serial.print(servoId);
//                Serial.print(" | ");
//                Serial.print(angle);
//                Serial.print(" => ");
//                Serial.println(pulselength);

                pwm.setPWM(servoId, 0, pulselength);
        }
        void moveToUnit(float unit) {
                int pulselength = getPulseLengthForUnit(unit);

//                Serial.print(servoId);
//                Serial.print(" | ");
//                Serial.print(unit);
//                Serial.print(" => ");
//                Serial.println(pulselength);

                pwm.setPWM(servoId, 0, pulselength);
        }

  private:
        int getPulseLengthForDegrees(int degrees) {
                return map(degrees, 0, 180, SERVOMIN, SERVOMAX);
        }
        
        float getPulseLengthForUnit(double perlin) {
                return mapDoubles(perlin, -1, 1, SERVOMIN, SERVOMAX);
        }
        
        double mapDoubles(double x, double in_min, double in_max, double out_min, double out_max) {
                double temp = (x - in_min) * (out_max - out_min) / (in_max - in_min) + out_min;
                temp = (int) (4*temp + .5);
                return (double) temp/4;
        }
};

#endif /* MOVABLE_H_ */
