/*
 * Performable.h
 *
 *  Created on: Jul 2, 2015
 *      Author: resah
 * Description: Interface for all output variants
 */

#ifndef PERFORMABLE_H_
#define PERFORMABLE_H_

#include <Arduino.h>
#include "Temper.h"

class Performable {
  protected:
        byte updateInterval = 100;     // interval between updates
        unsigned long lastUpdate;      // last update of position

  public:
        virtual void init() = 0;
        virtual void setTemper(Temper &temper) = 0;
        virtual void perform() = 0;

        void performIfQueued() {
                if ((millis() - lastUpdate) > updateInterval) {
                        lastUpdate = millis();
                        perform();
                }
        }
};

#endif /* PERFORMABLE_H_ */
