//#include <Wire.h>
//#include <Adafruit_PWMServoDriver.h>
#include <TinyWireM.h>
#include <Adafruit_PWMServoDriverForTrinket.h>

#include "Performable.h"
#include "Movable.h"
#include "Noisable.h"
#include "BodyMover.h"
#include "Temper.h"
#include "Perlin.h"

#define SERVO_LR_MOVER 1
#define SERVO_UD_MOVER 0

Adafruit_PWMServoDriverForTrinket pwm = Adafruit_PWMServoDriverForTrinket();
//Adafruit_PWMServoDriver pwm = Adafruit_PWMServoDriver();

// output
BodyMover lrMover(pwm, SERVO_LR_MOVER);
BodyMover udMover(pwm, SERVO_UD_MOVER);

void setup()
{
        //Serial.begin(9600);
        
        pwm.begin();
        pwm.setPWMFreq(60);
        
	      lrMover.init();
	      udMover.init();
}

void loop()
{
        // TODO: voice for different mood synced with jaw

	      // 1. we run LR-Movement, UD-Movement, Voice (all handling mood)
	      // These actions should be random. Supposed to look natural.
	      lrMover.performIfQueued();
	      udMover.performIfQueued();

	      // 2. Sensors (Hall, Capacity) can change normal behavior:
	      // Analyze input and decide how to react.

        // 3. Inform everything about current mood
        Temper temper = neutral;
	      lrMover.setTemper(temper);
	      udMover.setTemper(temper);
}

