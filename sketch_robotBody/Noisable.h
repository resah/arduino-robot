/*
 * Noisable.h
 *
 *  Created on: Jun 29, 2015
 *      Author: resah
 * Description: Provides methods to generate Perlin noise.
 */

#ifndef NOISABLE_H_
#define NOISABLE_H_

#include <Arduino.h>
#include <math.h>
#include "Perlin.h"

#define SPEED 0.01

class Noisable {

  private:
        Perlin perlin;
        float perlinStep = 0;

  public:
        Noisable(byte &offset) {
              perlinStep = offset;
        }

  protected:
        double getNextNoiseValue() {
                perlinStep += SPEED;
                return perlin.noise(
                        perlinStep + sin(perlinStep), 
                        perlinStep + cos(perlinStep), 
                        perlinStep);
        }
};

#endif /* NOISABLE_H_ */
