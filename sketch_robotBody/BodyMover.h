/*
 * BodyMover.h
 *
 *  Created on: Jun 29, 2015
 *      Author: resah
 * Description: Controller for body movement
 */

#ifndef BODYMOVER_H_
#define BODYMOVER_H_

#include <Arduino.h>
#include <math.h>
#include "Performable.h"
#include "Movable.h"
#include "Noisable.h"
#include "Temper.h"
#include "Perlin.h"

class BodyMover: 
  public Performable,
  public Movable,
  public Noisable {

  public:
        BodyMover(Adafruit_PWMServoDriverForTrinket &servoDriver, byte servoIdentifier):
          Movable(servoDriver, servoIdentifier),
          Noisable(servoIdentifier) {
        }

	      void init() {
	      }

        void setTemper(Temper &temper) {
                if (temper == good) {
                        updateInterval = 50;
                } else if (temper == bad) {
                        updateInterval = 200;
                } else {
                        updateInterval = 100;
                }
        }

	      void perform() {
                float noiseValue = getNextNoiseValue();
                moveToUnit(noiseValue);
	      }
};

#endif /* BODYMOVER_H_ */
