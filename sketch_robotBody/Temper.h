/*
 * Temper.h
 *
 *  Created on: Jun 29, 2015
 *      Author: resah
 * Description: Enum type of possible mood states.
 */

#ifndef TEMPER_H_
#define TEMPER_H_

#include <Arduino.h>

enum Temper {
  neutral,
  good,
  bad
};

Temper temperOfIndex(int i) {
      if (i >= 3 || i < 0) {
              return neutral;
      }
      return static_cast<Temper>(i);
}

#endif /* TEMPER_H_ */
